#include <stdio.h>
#include <stdlib.h>

struct data //data in nodes of list
{
  int number;
};

/**
  * One-direction list (list)
  s*/

struct list //nodes of list
{
  struct data inf;
  struct list *next;
};

struct list *add_tohead (struct list *prhead, struct data *newdata);
struct list *add_totail (struct list *prhead, struct data *newdata);
void printlist (struct list *toprint);
int equals_byindex (struct list *a, int x, int y); //check equality for x'th and y'th elements in list (return 1 in case of equality, 0 - inequality, -1 - error)
int equals_tovalue (struct list *prhead, struct data *inf);
int find (struct list *head, struct data *tofind, int comparator(struct list *, struct data *)); //return first occurrence index, in case of fail - return -1
struct list *remove_byvalue (struct list *prhead, struct data *inf, int comparator(struct list *, struct data *));
struct list *remove_byindex (struct list *prhead, int ind);

/**
  * Two-direction list (blist)
  */

struct blist //nodes of two-directories list
{
  struct blist *next;
  struct blist *prev;
  struct data inf;
};

struct blist *add_tohead_b (struct blist *prhead, struct data *newdata);
struct blist *add_totail_b (struct blist *prhead, struct data *newdata);
void printlist_b (struct blist *toprint);
int equals_byindex_b (struct blist *a, int x, int y); //check equality for x'th and y'th elements in list (return 1 in case of equality, 0 - inequality, -1 - error)
int equals_tovalue_b (struct blist *prhead, struct data *inf);
int find_b (struct blist *head, struct data *tofind, int comparator(struct blist *, struct data *)); //return first occurrence index, in case of fail - return -1
struct blist *remove_byvalue_b (struct blist *prhead, struct data *inf, int comparator(struct blist *, struct data *));
struct blist *remove_byindex_b (struct blist *prhead, int ind);


int
main (void)
{
  //experimental
  struct blist *blst = NULL;
  struct data d;
  for (int i = 0; i <= 10; i+=2)
  {
    d.number = i;
    blst = add_totail_b(blst, &d);
    d.number = i+1;
    blst = add_tohead_b(blst, &d);
  }
  printlist_b (blst);
  while (blst->next)
  {
    blst = blst->next;
  }
  while (blst)
  {
    printf("%d ", blst->inf.number);
    blst = blst -> prev;
  }
  printf ("\n");
  //
  struct list *lst = NULL; //list is empty at the beginning
  char com = ' '; //space is initializing command
  int num; //used for parameters of commands
  int x,y;
  printf("--- list implementation ---\ncommands:\nt - add_totail\nh - add_tohead\np - print list\nc - equals\nf - find\nr - remove_byvalue\ni - remove_byindex\ne - exit\n\n");
  while (com!='e') //e - command to exit
  {
    switch (com)
    {
  case 't':
      scanf("%d", &num);
      struct data n = {.number = num};
      lst = add_totail(lst, &n);
      scanf("%c", &com);
      break;
    case 'h':
      {
      scanf("%d", &num);
      struct data m = {.number = num};
      lst = add_tohead(lst, &m);
      scanf("%c", &com);
      }
      break;
    case 'p':
      printlist(lst);
      scanf("%c", &com);
      break;
    case 'c':
      if (scanf("%d%d", &x, &y)==2)
        printf ("equality: %d\n", equals_byindex(lst, x, y));
      else
        printf ("illegal parameters\n");
      scanf("%c", &com);
      break;
    case 'f':
    {
      scanf ("%d", &num);
      struct data m = {.number = num};
      printf ("results of finding: %d\n", find (lst, &m, &equals_tovalue));
      scanf("%c", &com);
    }
      break;
    case 'r':
    {
      scanf ("%d", &num);
      struct data m = {.number = num};
      lst = remove_byvalue (lst, &m, equals_tovalue);
      scanf("%c", &com);
    }
      break;
    case 'i':
    {
      scanf ("%d", &num);
      lst = remove_byindex (lst, num);
      scanf("%c", &com);
      break;
    }
    case ' ':
      printf("print command\n");
      break;
    case '\n':
      printf("print command\n");
      break;
    default:
      printf("print command\n");
      scanf("%c", &com);
    }
    scanf("%c", &com);
  }
  //free memory for lst!!!
  return 0;
}

struct list
*add_tohead (struct list *prhead, struct data *newdata)
{
  struct list *nhead = malloc (sizeof(struct list));
  nhead->next = prhead;
  nhead->inf = *newdata;
  return nhead;
}

struct list
*add_totail (struct list *prhead, struct data *newdata)
{
  struct list *ntail = malloc (sizeof(struct list));
  ntail->inf = *newdata;
  ntail->next = NULL;
  if (!prhead)
  {
    return ntail;
  }
  struct list *savedhead = prhead;
  while (prhead->next)
  {
    prhead = prhead->next;
  }
  prhead->next = ntail;
  return savedhead;
};

void
printlist (struct list *toprint) //!for current data implementation (with number)
{
  printf("list: ");
  if (!toprint)
  {
    printf ("empty list\n");
    return;
  }
  while (toprint->next)
  {
    struct data f = toprint->inf;
    printf("%d ", f.number);
    toprint = toprint->next;
  }
  struct data f = toprint->inf;
  printf("%d ", f.number);
  printf("\n");
}

int
equals_byindex (struct list *a, int x, int y) //!for current data implementation (with number)
{
  if (!a)
    return -1;
  x--;
  y--;
  struct list *b = a;
  while (a->next && x)
  {
    a = a->next;
    x--;
  }
  while (b->next && y)
  {
    b = b->next;
    y--;
  }
  if (!x && !y)
  {
    if (a->inf.number == b->inf.number)
      return 1;
    else
      return 0;
  }
  else
    return -1;
}

int
equals_tovalue (struct list *prhead, struct data *inf) //!for current data implementation (with number)
{
  if (prhead->inf.number == inf->number)
    return 1;
  else
    return 0;
}

int
find (struct list *head, struct data *tofind, int comparator(struct list *, struct data *))
{
  if (!head)
    return -1;
  int ind = 1;
  while (head->next && !comparator(head, tofind))
  {
    head = head->next;
    ind++;
  }
  if (comparator(head,tofind))
    return ind;
  else
    return -1;
}

struct list
*remove_byvalue (struct list *prhead, struct data *inf, int comparator(struct list *, struct data *))
{
  struct list *prev = NULL;
  struct list *ph = prhead;
  while (prhead && !comparator(prhead, inf))
  {
    prev = prhead;
    prhead = prhead -> next;
  }
  if (!prhead)
    return ph;
  if (prev)
    prev -> next = prhead -> next;
  else
    ph = ph -> next;
  free (prhead);
  return ph;
}

struct list
*remove_byindex (struct list *prhead, int ind)
{
  struct list *s = prhead;
  struct list *prev = NULL;
  int x = 1;
  while (prhead -> next && x<ind)
  {
    prev = prhead;
    prhead = prhead -> next;
    x++;
  }
  if (x == ind)
  {
    if (!s)
      return s;
    if (!prev)
    {
      s = prhead -> next;
      free(prhead);
      return s;
    }
    prev -> next = prhead -> next;
    free(prhead);
    return s;
  }
  return s;
};

struct blist
*add_tohead_b (struct blist *prhead, struct data *newdata)
{
  struct blist *nhead = malloc (sizeof(struct blist));
  nhead->next = prhead;
  nhead->prev = NULL;
  nhead->inf = *newdata;
  if (prhead)
    prhead->prev = nhead;
  return nhead;
};

struct blist
*add_totail_b (struct blist *prhead, struct data *newdata)
{
  struct blist *ntail = malloc (sizeof(struct blist));
  ntail->inf = *newdata;
  ntail->next = NULL;
  ntail->prev = NULL;
  if (!prhead)
  {
    return ntail;
  }
  struct blist *savedhead = prhead;
  while (prhead->next)
  {
    prhead = prhead->next;
  }
  prhead->next = ntail;
  ntail->prev = prhead;
  return savedhead;
};

void
printlist_b (struct blist *toprint) //!for current data implementation (with number)
{
  printf("list: ");
  if (!toprint)
  {
    printf ("empty list\n");
    return;
  }
  while (toprint->next)
  {
    struct data f = toprint->inf;
    printf("%d ", f.number);
    toprint = toprint->next;
  }
  struct data f = toprint->inf;
  printf("%d ", f.number);
  printf("\n");
}

int
equals_byindex_b (struct blist *a, int x, int y)
{
  if (!a)
    return -1;
  x--;
  y--;
  struct blist *b = a;
  while (a->next && x)
  {
    a = a->next;
    x--;
  }
  while (b->next && y)
  {
    b = b->next;
    y--;
  }
  if (!x && !y)
  {
    if (a->inf.number == b->inf.number)
      return 1;
    else
      return 0;
  }
  else
    return -1;
}

int
equals_tovalue_b (struct blist *prhead, struct data *inf)
{
  if (prhead->inf.number == inf->number)
    return 1;
  else
    return 0;
}

int
find_b (struct blist *head, struct data *tofind, int comparator(struct blist *, struct data *))
{
  if (!head)
    return -1;
  int ind = 1;
  while (head->next && !comparator(head, tofind))
  {
    head = head->next;
    ind++;
  }
  if (comparator(head,tofind))
    return ind;
  else
    return -1;
}

struct blist
*remove_byvalue_b (struct blist *prhead, struct data *inf, int comparator(struct blist *, struct data *))
{
  struct blist *prev = NULL;
  struct blist *ph = prhead;
  while (prhead && !comparator(prhead, inf))
  {
    prev = prhead;
    prhead = prhead -> next;
  }
  if (!prhead)
    return ph;
  if (prev)
  {
    prev -> next = prhead -> next;
    prhead -> prev = prev;
  }
  else
  {
    ph = ph -> next;
    ph -> prev = NULL;
  }
  free (prhead);
  return ph;
};

struct blist
*remove_byindex_b (struct blist *prhead, int ind)
{
  struct blist *s = prhead;
  struct blist *prev = NULL;
  int x = 1;
  while (prhead -> next && x<ind)
  {
    prev = prhead;
    prhead = prhead -> next;
    x++;
  }
  if (x == ind)
  {
    if (!s)
      return s;
    if (!prev)
    {
      s = prhead -> next;
      s -> prev = NULL;
      free(prhead);
      return s;
    }
    prev -> next = prhead -> next;
    prhead -> prev = prev;
    free(prhead);
    return s;
  }
  return s;
};
